<?php
/**
 * Template Name: Full Width Page
 * Template Post Type: lp,page,post,braces_archive,invisalign_archive,technology_archive,accelerated_archive,services, medical_archive, cosmetic_archive, patient_resources, additional_treatment, braces, invisalign, your_providers
 * @package tabby
 * @since 1.0.0
 */
get_header();
get_template_part('template-parts/banner')
?>
<div class="full-width-page-template">
	<?php
		if(have_posts()){
			while ( have_posts() ) : the_post();
				the_content();
			endwhile;
		}
		wp_reset_postdata();
	?>
</div>
<?php get_footer();?>
