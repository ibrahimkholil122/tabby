<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tabby
 */
?>

	</div><!-- #content -->
	<div class="locations" id="locations">
		<?php $locations = carbon_get_theme_option('tabby_location_footer_style'); ?>
		<?php if ( $locations != "no_locations" ) : ?>
			<?php if ( $locations == 'without_interactive_map' && method_exists('Hip\Location\Frontend', 'renderMultiLocationWithoutMap')) : ?>
				<?php echo do_shortcode("[location_module_without_map]"); ?>
			<?php endif; ?>
			<?php if ( $locations == 'interactive_map') : ?>
				<?php echo do_shortcode("[location_module]"); ?>
			<?php endif; ?>
		<?php endif; ?>
	</div>
	<footer id="colophon" class="site-footer">
		<?php
			if(!is_404()):
			$subscribeFormLayout = carbon_get_theme_option('tabby_subscribe_form_layout');
			if($subscribeFormLayout !== 'none'):
		?>
		<div class="top-footer <?php echo $subscribeFormLayout;?>">
			<div class="container">
				<div class="row">
					<div class="col">
						<?php if ( is_active_sidebar( 'above_footer' ) ) : ?>
							<?php dynamic_sidebar( 'above_footer' ); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; endif; ?>
		<div class="main-footer">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-6 col-md-3 footer-col-1">
						<?php if ( is_active_sidebar('footer_col_1')) : ?>
							<?php dynamic_sidebar('footer_col_1'); ?>
						<?php endif; ?>
					</div>
					<div class="col-12 col-sm-6 col-md-3 footer-col-2">
						<?php if ( is_active_sidebar('footer_col_2')) : ?>
							<?php dynamic_sidebar('footer_col_2'); ?>
						<?php endif; ?>
					</div>
					<div class="col-12 col-sm-6 col-md-3 footer-col-3">
						<?php if ( is_active_sidebar( 'footer_col_3')) : ?>
							<?php dynamic_sidebar('footer_col_3'); ?>
						<?php endif; ?>
					</div>
					<div class="col-12 col-sm-6 col-md-3 footer-col-4">
						<?php if ( is_active_sidebar('footer_col_4' )) : ?>
							<?php dynamic_sidebar('footer_col_4'); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="sub-footer">
			<div class="container">
				<div class="row">
					<div class="col">
						<?php if ( is_active_sidebar( 'sub_footer' ) ) : ?>
							<?php dynamic_sidebar( 'sub_footer' ); ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
