<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tabby
 */
global $tabbyFields;
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="archive-staff">
				<div class="container">
					<?php
					$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					query_posts(array(
						'post_type' => 'staff',
						'paged' => $paged,
						'posts_per_page' => 6
					));
					?>
					<div class="row my-5">
						<?php if (have_posts() ): while (have_posts() ):the_post(); ?>
							<?php if(!empty($tabbyArchiveStaffLayout = $tabbyFields['tabby_staff_archive_layout'])): ?>
								<?php if($tabbyArchiveStaffLayout == 'layout-1'): ?>
									<div class="col-12 col-md-4 my-3">
										<div class="staff-member-item staff-layout-1 text-center border-bottom">
												<?php if(has_post_thumbnail()):the_post_thumbnail('thumbnail', array(
													'class'=>'img-fluid rounded-circle mb-4',
													'alt' => 'staff-img'
												)); ?>
												<?php endif; ?>
												<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
												<?php
												if(get_the_excerpt()):
													echo wp_trim_words( get_the_excerpt(), 20, '...' );
												else:
													echo wp_trim_words( get_the_content(), 20, '...' );
												endif;
												?>
												<div class="read-more"><a href="<?php the_permalink(); ?>">Read more</a></div>
										</div>
									</div>
								<?php elseif($tabbyArchiveStaffLayout == 'layout-2'): ?>
									<div class="col-12 col-md-4 my-3">
										<div class="staff-member-item staff-layout-2">
										<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>											<div class="staff-member-photo" style="background-image: url(<?php echo $url; ?>);"></div>
											<div class="staff-content">
												<div class="heading">
													<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
												</div>
												<div class="content">
													<?php
													if(get_the_excerpt()):
														echo wp_trim_words( get_the_excerpt(), 18, '...' );
													else:
														echo wp_trim_words( get_the_content(), 18, '...' );
													endif;
													?>
												</div>
												<div class="read-more"><a href="<?php the_permalink(); ?>">Read more</a></div>
											</div>
										</div>
									</div>
								<?php else: ?>
									<div class="col-12 my-2 my-md-0">
										<div class="staff-member-item mb-4">
											<div class="row">
												<?php if(has_post_thumbnail()):?>
													<div class="col-12 col-md-5 col-lg-4">
														<div class="staff-img text-center text-md-left">
															<?php the_post_thumbnail('full', array(
																'class'=>'img-fluid rounded'
															)); ?>
														</div>
													</div>
													<div class="col-12 col-md-7 col-lg-8">
														<div class="staff-content text-center text-md-left py-3">
															<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
															<?php
															if(get_the_excerpt()):
																echo wp_trim_words( get_the_excerpt(), 50, '...' );
															else:
																echo wp_trim_words( get_the_content(), 50, '...' );
															endif;
															?>
															<div class="read-more pt-3 pb-0"><a href="<?php the_permalink(); ?>">Read more</a></div>
														</div>
													</div>
												<?php else: ?>
													<div class="col-12">
														<div class="staff-content text-center text-md-left py-3">
															<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
															<?php
															if(get_the_excerpt()):
																echo wp_trim_words( get_the_excerpt(), 50, '...' );
															else:
																echo wp_trim_words( get_the_content(), 50, '...' );
															endif;
															?>
															<div class="read-more pt-3 pb-0"><a href="<?php the_permalink(); ?>">Read more</a></div>
														</div>
													</div>
												<?php endif; ?>
											</div>
										</div>
										<hr>
									</div>
								<?php endif; ?>
							<?php endif; ?>
							<?php
							?>
						<?php endwhile; ?>
					</div>
					<div class="row">
						<div class="col">
							<div class="archive-pagination text-center my-3">
								<?php the_posts_pagination(array( 'screen_reader_text'=> '&nbsp;')); ?>
							</div>
						</div>
					</div>
					<?php endif;?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_footer();
