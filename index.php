<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tabby
 */

get_header();

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts(array(
	'post_type' => 'post',
	'paged' => $paged,
	'posts_per_page' => 5
));
?>
	<div class="container">
		<div id="primary" class="content-area">
			<main id="main" class="site-main">
				<div class="blog-posts-wrapper my-5">
					<div class="row">
					<?php echo ((is_active_sidebar('post_sidebar')) ? '<div class="col-12 col-md-7 col-lg-8">' : '<div class="col-12 col-md-12 col-lg-12">'); ?>
							<div class="row">
							<?php if (have_posts() ) : if ( is_home() && ! is_front_page() ) :
							global $post;
							$counter = 1;
							while (have_posts() ) : the_post(); ?>
								<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
								<?php if($counter == 1): ?>
									<?php if($paged !== 1):?>
										<div class="col-12 col-lg-6 mb-4">
											<div class="post-content border-bottom">
												<?php if(has_post_thumbnail()):?>
												<div class="grid-post-img rounded" style="background-image: url(<?php echo $url; ?>);"></div>
												<?php endif; ?>
												<h4 class="post-heading pt-3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
												<ul class="post-meta">
													<li class="list-inline-item"><?php _e('by ','tabby'); ?><a href="<?php the_permalink(); ?>" class="primary-txt"><?php the_author(); ?></a></li>
													<li class="list-inline-item"><a href="<?php the_permalink(); ?>" class="primary-txt"><?php the_time('F j, Y'); ?></a></li>
												</ul>
												<div class="content py-3">
													<?php
													if(get_the_excerpt()):
														echo wp_trim_words( get_the_excerpt(), 30, '...' );
													else:
														echo wp_trim_words( get_the_content(), 30, '...' );
													endif;
													?>
												</div>
												<div class="wp-block-button tabby-block-btn primary small with-shadow mt-3">
													<a class="wp-block-button__link" href="<?php the_permalink(); ?>"><?php _e('Read More', 'tabby'); ?></a>
												</div>
											</div>
										</div>
									<?php else: ?>
										<div class="col-12 mb-4">
											<div class="post-content border-bottom">
												<?php if(has_post_thumbnail()):?>
												<div class="post-img rounded" style="background-image: url(<?php echo $url; ?>)"></div>
												<?php endif; ?>
												<h4 class="post-heading pt-3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
												<ul class="post-meta">
													<li class="list-inline-item"><?php _e('by ','tabby'); ?><a href="<?php the_permalink(); ?>" class="primary-txt"><?php the_author(); ?></a></li>
													<li class="list-inline-item"><a href="<?php the_permalink(); ?>" class="primary-txt"><?php the_time('F j, Y'); ?></a></li>
												</ul>
												<div class="content py-3">
													<?php
													if(get_the_excerpt()):
														echo wp_trim_words( get_the_excerpt(), 50, '...' );
													else:
														echo wp_trim_words( get_the_content(), 50, '...' );
													endif;
													?>
												</div>
												<div class="wp-block-button tabby-block-btn primary small with-shadow mt-3">
													<a class="wp-block-button__link" href="<?php the_permalink(); ?>"><?php _e('Read More', 'tabby'); ?></a>
												</div>
											</div>
										</div>
									<?php endif; ?>

								<?php endif; ?>
								<?php if($counter > 1): ?>
									<div class="col-12 col-lg-6 mb-4">
										<div class="post-content border-bottom">
											<?php if(has_post_thumbnail()): ?>
											<div class="grid-post-img rounded" style="background-image: url(<?php echo $url; ?>);"></div>
											<?php endif; ?>
											<h4 class="post-heading pt-3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
											<ul class="post-meta">
												<li class="list-inline-item"><?php _e('by ','tabby'); ?><a href="<?php the_permalink(); ?>" class="primary-txt"><?php the_author(); ?></a></li>
												<li class="list-inline-item"><a href="<?php the_permalink(); ?>" class="primary-txt"><?php the_time('F j, Y'); ?></a></li>
											</ul>
											<div class="content py-3">
												<?php
												if(get_the_excerpt()):
													echo wp_trim_words( get_the_excerpt(), 30, '...' );
												else:
													echo wp_trim_words( get_the_content(), 30, '...' );
												endif;
												?>
											</div>
											<div class="wp-block-button tabby-block-btn primary small with-shadow mt-3">
												<a class="wp-block-button__link" href="<?php the_permalink(); ?>"><?php _e('Read More', 'tabby'); ?></a>
											</div>
										</div>
									</div>
								<?php endif; ?>
							<?php $counter++; endwhile; endif; wp_reset_postdata(); endif; ?>
							</div>
							<div class="row">
								<div class="col">
									<div class="archive-pagination text-center my-3">
										<?php the_posts_pagination(array( 'screen_reader_text'=> '&nbsp;')); ?>
									</div>
								</div>
							</div>
						</div>
						<?php if ( is_active_sidebar('post_sidebar')) : ?>
							<div class="col-12 col-md-5 col-lg-4">
								<div class="post-sidebar">
										<?php dynamic_sidebar('post_sidebar'); ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</main><!-- #main -->
		</div><!-- #primary -->
	</div>
<?php
get_footer();
