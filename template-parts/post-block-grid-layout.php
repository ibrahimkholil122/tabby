<?php
/**
 * Hip Post Block  grid layout
 */
?>
<div class="hip-post-block-wrapper layout-grid">
	<div class="row">
		<?php foreach ($postBlockPosts as $postBlockPost):?>
			<div class="col-12 col-sm-6 col-md-4">
				<div class="hip-post-block-post text-center text-md-left">
					<?php if(has_post_thumbnail($postBlockPost->ID)):?>
						<div class="post-image" style="background-image: url('<?php echo get_the_post_thumbnail_url($postBlockPost->ID,'medium')?>')"></div>
					<?php endif;?>
					<div class="post-content">
						<h2><?php echo $postBlockPost->post_title;?></h2>
						<p>
							<?php echo !empty($postBlockPost->post_excerpt) ? wp_trim_words($postBlockPost->post_excerpt,$excerptLength,'') : wp_trim_words($postBlockPost->post_content, $excerptLength,''); ?>
						</p>
					</div>
					<div class="post-read-more">
						<a href="<?php echo get_the_permalink($postBlockPost->ID);?>" class="primary-txt primary-txt-hover">Read More</a>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>