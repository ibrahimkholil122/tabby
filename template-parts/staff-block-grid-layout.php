<?php
/**
 * Staff gutenberg block grid layout
 */
?>
<div class="hip-staff-block-wrapper layout-grid">
	<div class="row">
		<?php foreach ($staffs as $staff):?>
			<div class="col-12 col-sm-6 col-md-4">
				<div class="hip-staff-block-staff text-center text-md-left">
					<?php if(has_post_thumbnail($staff->ID)):?>
						<div class="staff-image" style="background-image: url('<?php echo get_the_post_thumbnail_url($staff->ID,'medium')?>')"></div>
					<?php endif;?>
					<div class="staff-content">
						<h2><?php echo $staff->post_title;?></h2>
						<p>
							<?php echo !empty($staff->post_excerpt) ? $staff->post_excerpt : wp_trim_words($staff->post_content,20,''); ?>
						</p>
					</div>
					<div class="staff-read-more">
						<a href="<?php echo get_the_permalink($staff->ID);?>" class="button-primary">Read More</a>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
