<?php
/**
 * Template part for displaying page Banner Layout 3 *
 * @package tabby
 * @since 1.0.0
 */
global $tabbyFields;
$featuredImg = get_the_post_thumbnail_url($post,'full');
$archiveBanner = $tabbyFields['tabby_archive_banner_image'];
$Url = wp_get_attachment_image_src($archiveBanner, 'full');
$imageSrc = '';
if($featuredImg){
	$imageSrc =  $featuredImg;
}else{
	$imageSrc =  $Url[0];
}
?>
<div class="banner banner-layout-3" <?php echo !empty($imageSrc) ? 'style="background-image:url('.$imageSrc.')"':'';?>>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-12">
				<div class="banner-content text-center text-md-left text-white">
					<?php if(is_archive()): ?>
						<h1><?php echo post_type_archive_title( '', false ); ?></h1>
					<?php elseif(is_home()): ?>
						<h1><?php _e('Blog','tabby'); ?></h1>
					<?php else: ?>
						<h1><?php echo get_the_title($post->ID);?></h1>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_template_part('template-parts/breadcrumb-layout-3');?>