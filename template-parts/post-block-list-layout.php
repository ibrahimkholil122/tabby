<?php
/**
 * Hip Post Block grid layout
 */
?>
<div class="hip-post-block-wrapper layout-list">
	<?php foreach ($postBlockPosts as $key=>$postBlockPost):?>
		<?php $key++;?>
		<div class="hip-post-block-post">
			<div class="row align-items-center <?php echo $key % 2 == 0 ? 'flex-md-row-reverse':''?>">
				<div class="col-12 col-md-6 left">
					<?php if(has_post_thumbnail($postBlockPost->ID)):?>
						<div class="post-image" style="background-image: url('<?php echo get_the_post_thumbnail_url($postBlockPost->ID,'large')?>')"></div>
					<?php endif;?>
				</div>
				<div class="col-12 col-md-6 right">
					<div class="post-content-wrapper">
						<div class="post-info">
							<h2><?php echo $postBlockPost->post_title;?></h2>
							<p>
								<?php echo !empty($postBlockPost->post_excerpt) ? wp_trim_words($postBlockPost->post_excerpt,$excerptLength,'') : wp_trim_words($postBlockPost->post_content, $excerptLength,''); ?>
							</p>
						</div>
						<div class="post-read-more">
							<a href="<?php echo get_the_permalink($postBlockPost->ID);?>" class="button-primary-outline small-size">Read More</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>