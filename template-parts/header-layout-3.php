<?php
/**
 * Template part for displaying page Header Layout 3 *
 * @package tabby
 * @since 1.0.0
 */
global $tabbyFields;
?>
<header class="header header-layout-3">
	<div class="header-top-bar primary-highlight-background d-none d-lg-block">
		<div class="container h-100">
			<div class="row align-items-center h-100">
				<div class="col-auto">
					<div class="header-top-info">
						<p class="text-white mb-0">
							<a href="tel:<?php echo do_shortcode('[tabby-business-phone-number-for-links]');?>">
								<i class="fas fa-phone mr-2"> </i>
								<?php echo do_shortcode('[tabby-business-phone-number]');?>
							</a>
						</p>
					</div>
				</div>
				<div class="col-auto">
					<?php echo do_shortcode('[tabby-business-social-links]');?>
				</div>
				<div class="col ml-auto d-flex justify-content-end">
					<div class="tabby-top-menu">
						<?php
						if ( has_nav_menu( 'secondary' ) ) :
							wp_nav_menu( array(
								'menu'              => __( 'Secondary Nav', 'tabby'),
								'container'       => false,
								'theme_location'    => 'secondary',
								'depth'             => 4,
								'menu_class'        => 'secondary-nav'
							));
						endif;
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-main">
		<div class="container h-100">
			<div class="row align-items-center h-100">
				<div class="col-auto">
					<div class="logo">
						<a href="<?php get_bloginfo('url');?>">
							<?php echo do_shortcode('[tabby-logo]');?>
						</a>
					</div>
				</div>
				<div class="col">
					<div class="main-nav text-right d-none d-lg-block">
						<?php
						if ( has_nav_menu( 'main' ) ) :
							wp_nav_menu( array(
								'menu'              => __( 'Primary Nav', 'tabby'),
								'theme_location'    => 'main',
							));
						endif;
						?>
					</div>
					<div class="text-right call-now-btn d-block d-lg-none ">
						<?php if(!empty($tabbyMenuLayout = $tabbyFields['tabby_menu_layout'])): ?>
							<?php if($tabbyMenuLayout == 'hamburger'): ?>
								<div class="tab hamburger-btn-wrap">
									<button>
										<p class="c-hamburger c-hamburger--htx"><span></span></p>
									</button>
								</div>
							<?php else: ?>
								<a href="tel:<?php echo do_shortcode('[tabby-business-phone-number-for-links]');?>"><i class="fa fa-phone"></i><span class="d-none d-sm-inline-block ml-2">Call Now</span></a>
							<?php endif; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if(!empty($tabbyMenuLayout = $tabbyFields['tabby_menu_layout'])): ?>
		<?php if($tabbyMenuLayout == 'hamburger'): ?>
			<div class="hamburger-menu-wrap-continer d-block d-lg-none">
				<?php get_template_part('template-parts/hamburger-menu'); ?>
			</div>
		<?php endif; ?>
	<?php endif; ?>
</header>
