<?php global $tabbyFields; ?>
<div class="mobile-menu-wrap hamburger-menu-wrap <?php if(!empty($tabbyMenuLayout = $tabbyFields['tabby_alternate_menu_layout'])): ?><?php if($tabbyMenuLayout == 'yes'){echo "alternate-menu-style";}?><?php endif;?>">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php wp_nav_menu([ 'theme_location' => "main" ]); ?>
			</div>
		</div>
	</div>
</div>