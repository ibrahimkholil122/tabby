<?php
/**
 * Staff gutenberg block grid layout
 */
?>
<div class="hip-staff-block-wrapper layout-list">
	<?php foreach ($staffs as $key=>$staff):?>
		<?php $key++;?>
		<div class="hip-staff-block-staff">
			<div class="row align-items-center <?php echo $key % 2 == 0 ? 'flex-md-row-reverse':''?>">
				<div class="col-12 col-md-6 left">
					<?php if(has_post_thumbnail($staff->ID)):?>
						<div class="staff-image" style="background-image: url('<?php echo get_the_post_thumbnail_url($staff->ID,'large')?>')"></div>
					<?php endif;?>
				</div>
				<div class="col-12 col-md-6 right">
					<div class="staff-content-wrapper">
						<div class="staff-info">
							<h2><?php echo $staff->post_title;?></h2>
							<p>
								<?php echo !empty($staff->post_excerpt) ? $staff->post_excerpt : wp_trim_words($staff->post_content,45,''); ?>
							</p>
						</div>
						<div class="staff-read-more">
							<a href="<?php echo get_the_permalink($staff->ID);?>" class="button-primary-outline small-size">Read More</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>
