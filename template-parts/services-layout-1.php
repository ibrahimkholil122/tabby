<?php
global $conditionCount;
$reverseClass = '';
if($conditionCount % 2 == 0){
	$reverseClass = 'flex-row flex-md-row-reverse';
}
?>
<div class="row align-items-center single-condition <?php echo $reverseClass;?>">
	<?php if(has_post_thumbnail()): ?>
	<div class="col-12 col-md-6">
		<div class="condition-img-wrapper">
			<?php the_post_thumbnail('large');?>
		</div>
	</div>
	<?php endif;?>
	<div class="col-12 col-md-6">
		<div class="condition-content-wrapper">
			<h2 class="condition-title"><?php the_title();?></h2>
			<div class="condition-excerpt">
				<?php the_excerpt();?>
			</div>
			<a href="<?php the_permalink();?>" class="button-primary-outline small-size">Read More</a>
		</div>
	</div>
</div>