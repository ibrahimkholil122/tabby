<div class="col-12 col-sm-6 col-lg-4 single-condition">
	<div class="condition-content-wrapper">
		<h2 class="condition-title"><?php the_title();?></h2>
		<div class="condition-excerpt">
			<?php the_excerpt();?>
		</div>
		<div class="condition-link">
			<a href="<?php the_permalink();?>" class="button-primary-outline small-size">Read More</a>
		</div>
	</div>
</div>