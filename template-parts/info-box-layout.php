<?php
/**
 * Template for rendering info box Item
 */
?>
<div class="hip-info-box-wrapper-container">
	<div class="hip-info-box-carousel px-4 px-md-3 px-lg-3 px-xl-0 owl-carousel">
		<?php foreach ($infoBoxContent as $infoBox): ?>
			<div class="hip-info-box-wrapper">
				<div class="hip-info-box-inner" style="background: <?php echo $infoBox['info_bg_color']; ?>">
					<div class="hip-info-content">
						<?php if(!empty($infoBox['info_title'])): ?>
							<h4 style="color: <?php echo $infoBox['info_text_color']; ?>"><?php echo $infoBox['info_title']; ?></h4>
						<?php endif; ?>
						<?php if(!empty($infoBox['info_description'])): ?>
							<p style="color: <?php echo $infoBox['info_text_color']; ?>"><?php echo $infoBox['info_description']; ?></p>
						<?php endif; ?>
						<?php if(!empty($infoBox['info_link_label']) || !empty($infoBox['info_link'])): ?>
							<a style="color: <?php echo $infoBox['info_text_color']; ?>" href="<?php echo $infoBox['info_link'] ? $infoBox['info_link'] : '#'; ?>"><?php echo $infoBox['info_link_label'] ? $infoBox['info_link_label'] : 'Read More'; ?></a>
						<?php endif; ?>
						<?php if(!empty($infoBox['info_icon'])): ?>
						<span class="info-icon">
							<?php echo $infoBox['info_icon']; ?>
						</span>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
<style type="text/css">
	<?php if(!empty($fields['info_box_carousel_dot_hide'])): ?>
	.hip-info-box-wrapper-container .hip-info-box-carousel .owl-nav{
		display: none;
	}
	<?php endif; ?>
	<?php if(!empty($fields['info_box_carousel_nav_hide'])): ?>
	.hip-info-box-wrapper-container .hip-info-box-carousel .owl-dots{
		display: none;
	}
	<?php endif; ?>
	<?php if(!empty($fields['info_box_carousel_nav_hover_color'])): ?>
	.hip-info-box-wrapper-container .hip-info-box-carousel button.owl-next:hover i, .hip-info-box-wrapper-container .hip-info-box-carousel button.owl-prev:hover i {
		color: <?php echo $fields['info_box_carousel_nav_hover_color']; ?>;
	}
	<?php endif; ?>
	<?php if(!empty($fields['info_box_carousel_nav_color'])): ?>
	.hip-info-box-wrapper-container .hip-info-box-carousel button.owl-next i, .hip-info-box-wrapper-container .hip-info-box-carousel button.owl-prev i {
		color: <?php echo $fields['info_box_carousel_nav_color']; ?>;
	}
	<?php endif; ?>
	<?php if(!empty($fields['info_box_carousel_dot_color'])): ?>
	.hip-info-box-wrapper-container .hip-info-box-carousel .owl-dots button.owl-dot {
		background: <?php echo $fields['info_box_carousel_dot_color']; ?>;
	}
	<?php endif; ?>
	<?php if(!empty($fields['info_box_carousel_dot_active_color'])): ?>
	.hip-info-box-wrapper-container .hip-info-box-carousel .owl-dots button.owl-dot.active {
		background: <?php echo $fields['info_box_carousel_dot_active_color']; ?>;
	}
	<?php endif; ?>
</style>