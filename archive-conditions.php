<?php
/**
 * The template for displaying conditions archive pages
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 * @package tabby
 */
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="archive-conditions py-4">
				<div class="container">
					<?php
					$template = carbon_get_theme_option('tabby_conditions_archive_layout');
					$conditionCount = 0;
					query_posts(array(
						'post_type' => 'conditions',
						'posts_per_page' => -1,
						'orderby' => 'id',
						'order' => 'DESC'
					));
					?>
					<?php if($template == 'layout-1'): ?>
					<div class="row">
						<div class="col">
							<div class="conditions-<?php echo $template?>">
								<?php
								if(have_posts()):
									while(have_posts()): the_post();
										$conditionCount++;
										?>
										<?php get_template_part('template-parts/conditions-'.$template);?>
									<?php
									endwhile;
								endif;
								?>
							</div>
						</div>
					</div>
					<?php else: ?>
					<div class="row conditions-<?php echo $template?>">
						<?php
							if(have_posts()):
								while(have_posts()): the_post();
									$conditionCount++;
									?>
									<?php get_template_part('template-parts/conditions-'.$template);?>
								<?php
								endwhile;
							endif;
						?>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
if(carbon_get_theme_option('tabby_conditions_archive_testimonial') == 'yes'){
	echo do_shortcode('[hip_reviews]');
}
get_footer();
