<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tabby
 */

get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
			$postType = get_queried_object();
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			query_posts(array(
				'post_type' =>$postType->name,
				'paged' => $paged,
				'posts_per_page' => 20,
				'order' => 'ASC',
				'orderby' => 'menu_order'
			));
			$tabbyArchiveLayout = carbon_get_theme_option('tabby_default_archive_layout');
		?>
			<?php if($tabbyArchiveLayout == 'layout-1'): ?>
			<div class="archive-staff">
				<div class="container">
					<div class="row my-5">
						<?php if (have_posts() ): while (have_posts() ):the_post(); ?>
							<div class="col-12 col-md-4 my-3">
								<div class="staff-member-item staff-layout-2">
									<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
									<?php if(!empty($url)): ?>
									<div class="staff-member-photo" style="background-image: url(<?php echo $url; ?>);"></div>
									<?php endif; ?>
									<div class="staff-content">
										<div class="heading">
											<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
										</div>
										<div class="content">
											<?php
											if(get_the_excerpt()):
												echo wp_trim_words( get_the_excerpt(), 18, '...' );
											else:
												echo wp_trim_words( get_the_content(), 18, '...' );
											endif;
											?>
										</div>
										<div class="read-more"><a href="<?php the_permalink(); ?>">Read more</a></div>
									</div>
								</div>
							</div>
						<?php endwhile; endif; ?>
					</div>
				</div>
			</div>
			<?php elseif ($tabbyArchiveLayout == 'layout-2'): ?>
			<div class="archive-staff">
				<div class="container">
					<div class="row my-5">
						<?php if (have_posts() ): while (have_posts() ):the_post(); ?>
							<div class="col-12 my-2 my-md-0">
								<div class="staff-member-item mb-4">
									<div class="row">
										<?php if(has_post_thumbnail()):?>
										<div class="col-12 col-md-5 col-lg-4">
											<div class="staff-img text-center text-md-left">
												<?php the_post_thumbnail('full', array(
													'class'=>'img-fluid rounded'
												)); ?>
											</div>
										</div>
										<div class="col-12 col-md-7 col-lg-8">
											<div class="staff-content text-center text-md-left py-3">
												<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
												<?php
												if(get_the_excerpt()):
													echo wp_trim_words( get_the_excerpt(), 50, '...' );
												else:
													echo wp_trim_words( get_the_content(), 50, '...' );
												endif;
												?>
												<div class="read-more pt-3 pb-0"><a href="<?php the_permalink(); ?>">Read more</a></div>
											</div>
										</div>
										<?php else: ?>
										<div class="col-12">
											<div class="staff-content text-center text-md-left py-3">
												<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
												<?php
												if(get_the_excerpt()):
													echo wp_trim_words( get_the_excerpt(), 50, '...' );
												else:
													echo wp_trim_words( get_the_content(), 50, '...' );
												endif;
												?>
												<div class="read-more pt-3 pb-0"><a href="<?php the_permalink(); ?>">Read more</a></div>
											</div>
										</div>
										<?php endif; ?>
									</div>
								</div>
								<hr>
							</div>
						<?php endwhile; endif; ?>
					</div>
				</div>
			</div>
			<?php elseif ($tabbyArchiveLayout == 'layout-3'): ?>
			<div class="archive-conditions py-4">
				<div class="container">
					<div class="row">
						<div class="col">
							<div class="conditions-layout-1">
								<?php $conditionCount = 0; ?>
								<?php if(have_posts()): while(have_posts()): the_post(); ?>
									<?php $conditionCount++; ?>
									<?php get_template_part('template-parts/conditions-layout-1');?>
								<?php endwhile; endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php elseif ($tabbyArchiveLayout == 'layout-4'): ?>
			<div class="archive-conditions py-4">
				<div class="container">
					<div class="row conditions-layout-2">
						<?php $conditionCount = 0; ?>
						<?php if(have_posts()): while(have_posts()): the_post(); ?>
							<?php $conditionCount++; ?>
							<?php get_template_part('template-parts/conditions-layout-2');?>
						<?php endwhile; endif; ?>
					</div>
				</div>
			</div>
			<?php elseif ($tabbyArchiveLayout == 'layout-5'): ?>
			<div class="tabby-service-archive-wrapper">
				<div class="container">
					<div class="row">
						<div class="col">
							<?php $count = 1; ?>
							<?php if(have_posts()): while(have_posts()): the_post();?>
								<div class="tabby-service-post">
									<div class="row align-items-md-center <?php echo $count % 2 == 0 ? 'flex-md-row-reverse':''?>">
										<div class="col-12 col-sm-5 col-md-6 left">
											<?php if(has_post_thumbnail()): ?>
												<div class="tabby-service-image" style="background-image: url('<?php the_post_thumbnail_url('large');?>')"></div>
											<?php endif;?>
										</div>
										<div class="col-12 col-sm-7 col-md-6 right">
											<div class="tabby-service-content-wrapper">
												<div class="tabby-service-content">
													<h2 class="title"><?php the_title();?></h2>
													<p class="excerpt">
														<?php echo wp_trim_words(get_the_excerpt(get_the_ID()),40,'...'); ?>
													</p>
												</div>
												<div class="tabby-service-read-more">
													<a href="<?php the_permalink();?>" class="button-primary-outline small-size">Read More</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php $count++; endwhile; endif;?>
							</div>
						</div>
					</div>
				</div>
			<?php else: ?>
			<div class="archive-layout-6 py-4">
				<div class="container">
					<div class="row conditions-layout-2">
						<?php $conditionCount = 0; ?>
						<?php if(have_posts()): while(have_posts()): the_post(); ?>
							<div class="col-12 col-sm-6 col-lg-6 single-condition">
								<div class="condition-content-wrapper">
									<h2 class="condition-title"><?php the_title();?></h2>
									<div class="condition-excerpt">
										<?php if(get_the_excerpt()):
													echo wp_trim_words( get_the_excerpt(), 40, '...' );
												else:
													echo wp_trim_words( get_the_content(), 13, '...' );
												endif;?>
										<a href="<?php the_permalink();?>" class="button-primary-outline small-size">Read More</a>
									</div>
										
								</div>
							</div>
							<?php endwhile; endif; ?>
					</div>
				</div>
			</div>
			
			<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
