<?php
  global $tabbyFields;
?>
<?php if(!empty($tabbyHideWindowsLargerThan = $tabbyFields['tabby_hide_windows_larger_than'])):
	$windowWidth = strpos($tabbyHideWindowsLargerThan, 'px');
	if ($windowWidth == false): ?>
	@media screen and ( min-width: <?php echo $tabbyHideWindowsLargerThan + 1; ?>px ) {
		.tabbar-wrapper { display: none; }
	}
	<?php else: ?>
	@media screen and ( min-width: <?php echo $tabbyHideWindowsLargerThan + 1; ?> ) {
		.tabbar-wrapper { display: none; }
	}
<?php endif; endif; ?>
<?php if(!empty($tabbyHoverSelectedColor = $tabbyFields['tabby_hover_background_color'])): ?>
.tabbar {
	border-top: 1px solid <?php echo $tabbyHoverSelectedColor; ?>;
}
<?php endif; ?>
.tabbar .tab {
	<?php if(!empty($tabbyBackgroundColor = $tabbyFields['tabby_background_color'])): ?>
	background-color: <?php echo$tabbyBackgroundColor; ?>;
	<?php endif; ?>
	<?php if(!empty($tabbyTextLinkColor = $tabbyFields['tabby_text_link_color'])): ?>
	color: <?php echo $tabbyTextLinkColor; ?>;
	<?php endif; ?>
	<?php if(!empty($tabbyHoverSelectedColor = $tabbyFields['tabby_hover_background_color'])): ?>
	border-right: 1px solid <?php echo $tabbyHoverSelectedColor; ?>;
	<?php endif; ?>
}
.tabbar .tab button, .tabbar .tab button svg {
	<?php if(!empty($tabbyBackgroundColor = $tabbyFields['tabby_background_color'])): ?>
	background-color: <?php echo$tabbyBackgroundColor; ?>;
	<?php endif; ?>
	<?php if(!empty($tabbyTextLinkColor = $tabbyFields['tabby_text_link_color'])): ?>
	color: <?php echo $tabbyTextLinkColor; ?>;
	fill: <?php echo $tabbyTextLinkColor; ?>;
	<?php endif; ?>
}
.tabbar .tab, .tabbar .tab.selected button {
	<?php if(!empty($tabbyHoverSelectedColor = $tabbyFields['tabby_hover_background_color'])): ?>
	background-color: <?php echo $tabbyHoverSelectedColor; ?>;
	<?php endif; ?>
}
.c-hamburger span, .c-hamburger span:before, .c-hamburger span:after {
	<?php if(!empty($tabbyTextLinkColor = $tabbyFields['tabby_text_link_color'])): ?>
	background: <?php echo $tabbyTextLinkColor; ?>;
	<?php endif; ?>
}
.tabbar .tab div.tabbar-menu {
	<?php if(!empty($tabbyBackgroundColor = $tabbyFields['tabby_background_color'])): ?>
	background-color: <?php echo$tabbyBackgroundColor; ?>;
	<?php endif; ?>
}
.tabbar .tab div.tabbar-menu ul li a {
	<?php if(!empty($tabbyTextLinkColor = $tabbyFields['tabby_text_link_color'])): ?>
	color: <?php echo $tabbyTextLinkColor; ?>;
	<?php endif; ?>
}
.tabbar .tab div.tabbar-menu ul li {
	<?php if(!empty($tabbyTextLinkColor = $tabbyFields['tabby_text_link_color'])): ?>
	border-bottom: 1px solid <?php echo $tabbyTextLinkColor ?>;
	<?php endif; ?>
}
.tabbar .tab div.tabbar-menu  ul  li  ul {
	<?php if(!empty($tabbyHoverSelectedColor = $tabbyFields['tabby_hover_background_color'])): ?>
	background-color: <?php echo $tabbyHoverSelectedColor; ?>;
	<?php endif; ?>
}
.tabbar .tab div.tabbar-menu  ul  li  ul  li  ul {
	<?php if(!empty($tabbyBackgroundColor = $tabbyFields['tabby_background_color'])): ?>
	background-color: <?php echo$tabbyBackgroundColor; ?>;
	<?php endif; ?>
}
.tabbar .tab div.tabbar-menu .search-bar{
	<?php if(!empty($tabbyBackgroundColor = $tabbyFields['tabby_background_color'])): ?>
	background-color: <?php echo$tabbyBackgroundColor; ?>;
	<?php endif; ?>

}
<?php if(!empty($tabbySearchBackgroundColor = $tabbyFields['tabby_search_background_color'])) : ?>
.tabbar .tab div.tabbar-menu .search-bar input{
	background:  <?php echo $tabbySearchBackgroundColor; ?>;
}
<?php endif; ?>

.tabbar .tab div.tabbar-menu .search-bar #searchsubmit{
	color:  <?php echo !empty($tabbyFields['tabby_search_button_text_color']) ? $tabbyFields['tabby_search_button_text_color'] : !empty($tabbyFields['tabby_text_link_color'])  ?>;
}
.tabbar-wrapper .tabbar .tab div.tabbar-menu li.menu-item-has-children:after{
<?php if(!empty($tabbyTextLinkColor = $tabbyFields['tabby_text_link_color'])): ?>
	color: <?php echo $tabbyTextLinkColor ?>;
<?php endif; ?>
}
.tabbar-wrapper{
	display: none;
}
.tabbar-wrapper .tabbar .tab .tabbar-icon-wrap{
	width: 24px;
	height: 24px;
	margin: 0 auto 5px;
	display: block;
}
.tabbar-wrapper .tabbar .tab .tabbar-icon-wrap img{
	max-width: 100%;
}
.tabbar-wrapper .tabbar .tab .tabbar-icon-wrap .genericon-wrap{
	width: 100%;
	height: 100%;
}
.tabbar-wrapper .tabbar .tab .tabbar-icon-wrap .genericon-wrap svg{
	width: 100%;
	height: 100%;
	fill: #fff;
	background: transparent;
}
.tabbar-wrapper .tabbar .tab .tabbar-icon-wrap i{
	font-size: 20px;
}
body.logged-in.admin-bar .tabbar-wrapper .tabbar .tab.selected div.tabbar-menu{
	height: calc(100% - 94px);
}

<?php if(!empty($tabbyMenuLayout = $tabbyFields['tabby_menu_layout'])) :  if(!empty($tabbyMenuLayout == 'tabbar_top')): ?>
.tabbar-wrapper {
	top: 0;
	bottom: unset;
}
body.admin-bar > .tabbar-wrapper {
	top: 32px;
}
@media(max-width: 767px){
	body.admin-bar > .tabbar-wrapper {
		top: 46px;
	}
}
<?php if(!empty($tabbyHoverSelectedColor = $tabbyFields['tabby_hover_background_color'])): ?>
.tabbar {
	border-top: none;
	border-bottom: 1px solid <?php echo $tabbyHoverSelectedColor; ?>;
}
<?php endif; ?>
.tabbar-wrapper .tabbar .search-bar {
	padding: 30px 5px 55px;
}
body.logged-in.admin-bar > .tabbar-wrapper .tabbar .tab div.tabbar-menu{
	top: 94px;
}
@media(max-width: 767px){
	body.logged-in.admin-bar > .tabbar-wrapper .tabbar .tab div.tabbar-menu {
		top: 108px;
	}
}
.tabbar-wrapper .tabbar .tab div.tabbar-menu {
	top: 62px;
}

<?php endif;  endif; ?>

<?php
	$tabbarVisibleWidth = $tabbyFields['tabby_hide_windows_larger_than'];
	$mobileMenuLayout = $tabbyFields['tabby_menu_layout'];
?>
@media(max-width: <?php echo !empty($tabbarVisibleWidth) ? $tabbarVisibleWidth : '991';?>px){
	<?php if($mobileMenuLayout == 'tabbar_top'):?>
	body{
		padding-top: 60px;
	}
	<?php endif;?>
	<?php if($mobileMenuLayout == 'tabbar_bottom'):?>
		body{
			padding-bottom: 60px;
		}
	<?php endif;?>
	.tabbar-wrapper{
		display: block;
	}
}
