<?php
global $tabbyFields;
?>
<?php if (!empty($bodyFontColor = $tabbyFields['tabby_body_font_color'])) : ?>
	body,p,ul,ol,form input,form textarea,form select,form input[type="radio"], form input[type="checkbox"],blockquote,.fl-rich-text p{
	color : <?php echo $bodyFontColor; ?>
	}
<?php endif;?>
<?php if (!empty($tabbyLinkColor = $tabbyFields['tabby_link_color'])) : ?>
	a, .fl-builder-content .fl-button-wrap .fl-button{
		color: <?php echo $tabbyLinkColor; ?>
	}
<?php endif;?>
<?php if (!empty($tabbyLinkHoverColor = $tabbyFields['tabby_link_hover_color'])) : ?>
	a:hover, .fl-builder-content .fl-button-wrap .fl-button:hover,.fl-button-wrap a:focus{
		color: <?php echo $tabbyLinkHoverColor; ?>
	}
<?php endif;?>
<?php if (!empty($tabbyPrimaryColor = $tabbyFields['tabby_primary_color'])) : ?>
	.fl-menu > .menu > li > a,.fl-menu > .menu > li > .fl-has-submenu-container > a,.primary-heading .fl-module-content .fl-heading,.fl-module h1[class^="fl"][class$="title"],.fl-module h2[class^="fl"][class$="title"],.fl-module h3[class^="fl"][class$="title"],
	.fl-module h4[class^="fl"][class$="title"].fl-module h5[class^="fl"][class$="title"],.fl-module h6[class^="fl"][class$="title"],.fl-module h1,.fl-module h2,.fl-module h3,.fl-module h4,.fl-module h5,.fl-module h6,.text-primary .fl-rich-text p,.primary-txt,.text-primary .fl-module-content div[class^="fl-post"][class$="content"] p,.text-primary .fl-module-content div[class^="fl-post"][class$="excerpt"] p,.text-primary .fl-module-content div[class^="fl-"][class$="text"] p,h1,h2,h3,h4,h5,h6,header.header.header-layout-3 .header-main .main-nav ul li.main-menu-btn a, header.header.header-layout-4 .header-main .main-nav ul li.main-menu-btn a,.button-primary-outline,  .hip-staff-block-wrapper.layout-grid .hip-staff-block-staff .staff-read-more a.button-primary, .blog-posts-wrapper .post-content>.read-more a,body a.primary-txt-hover:hover, body button.primary-txt-hover:hover{
		color: <?php echo $tabbyPrimaryColor; ?>
	}
.primary-background,.primary-bg .fl-row-content-wrap,.button-primary,.fl-builder-content .primary-btn .fl-module-content .fl-button-wrap a.fl-button,.primary-btn .fl-module-content div[class^="fl-post-"] a[class^="fl-post-"][class$="more"],.primary-btn .fl-module-content div[class^="fl-post-"] .fl-post-text .fl-post-more-link a,.fl-builder-content div.primary-btn .fl-module-content form button[type="submit"],header.header .header-main ul.menu li.main-menu-btn a,.banner:before,.tabby-block-btn.primary a,.button-primary-outline:hover, .hip-review-single-wrapper .quote-icon span, footer.site-footer .top-footer, .archive-pagination nav.navigation.pagination .nav-links span,body.header-is-sticky.tabby-header-layout-4 header .header-main, body.header-is-sticky.tabby-header-layout-3 header .header-main,body.tabby-hamburger-menu.tabby-sticky-header.header-is-sticky header, header.header .header-main .call-now-btn a{
	background-color: <?php echo $tabbyPrimaryColor; ?>;
}
.booking-now-form .wpforms-submit-container button.wpforms-submit, .contact .wpforms-submit-container button.wpforms-submit{
	background-color: <?php echo $tabbyPrimaryColor; ?>!important;
}
.button-primary-outline{
	border-color:<?php echo $tabbyPrimaryColor; ?>
}
.wp-block-hvc-video-block-video-from-collection.video-block .pps-video-wrap .pps-video-preview-wrap a span.push-btn:after{
	border-left: 25px solid <?php echo $tabbyPrimaryColor; ?>;
}

article.search-results:hover {
	border-left: 10px solid <?php echo $tabbyPrimaryColor; ?>;
}

<?php endif;?>
<?php if (!empty($tabbyPrimaryHighlightColor = $tabbyFields['tabby_primary_highlight_color'])) : ?>
	.text-primary-highlight .fl-rich-text p, .primary-txt-highlight,.text-primary-highlight .fl-module-content div[class^="fl-post"][class$="content"] p,.text-primary-highlight .fl-module-content div[class^="fl-post"][class$="excerpt"] p,.text-primary-highlight .fl-module-content div[class^="fl-"][class$="text"] p{
		color: <?php echo $tabbyPrimaryHighlightColor; ?>
	}
.primary-highlight-background, .primary-bg-highlight .fl-row-content-wrap, .button-primary:hover, .button-primary:focus, .fl-builder-content .primary-btn .fl-module-content .fl-button-wrap a.fl-button:hover, .primary-btn .fl-module-content div[class^="fl-post-"] a[class^="fl-post-"][class$="more"]:hover,.primary-btn .fl-module-content div[class^="fl-post-"] .fl-post-text .fl-post-more-link a:hover,.fl-builder-content div.primary-btn .fl-module-content form button[type="submit"]:hover,.fl-builder-content .primary-btn .fl-module-content .fl-button-wrap a.fl-button:focus, .primary-btn .fl-module-content div[class^="fl-post-"] a[class^="fl-post-"][class$="more"]:focus,.primary-btn .fl-module-content div[class^="fl-post-"] .fl-post-text .fl-post-more-link a:focus,.fl-builder-content div.primary-btn .fl-module-content form button[type="submit"]:focus,header.header .header-main ul.menu li.main-menu-btn a:hover,.tabby-block-btn.primary-highlight a, .tabbar .tab div.tabbar-menu .search-bar input, header.header .header-main .call-now-btn:hover a{
	background-color: <?php echo $tabbyPrimaryHighlightColor; ?>;
}
.booking-now-form .wpforms-submit-container button.wpforms-submit:hover, .contact form.wpforms-form .wpforms-submit-container button.wpforms-submit:hover{
	background-color: <?php echo $tabbyPrimaryHighlightColor; ?>;
}
<?php endif;?>
<?php if (!empty($tabbySecondaryColor = $tabbyFields['tabby_secondary_color'])) : ?>
	.secondary-heading .fl-module-content .fl-heading,.fl-module.secondary-heading h1[class^="fl"][class$="title"],.fl-module.secondary-heading h2[class^="fl"][class$="title"],.fl-module.secondary-heading h3[class^="fl"][class$="title"],.fl-module.secondary-heading h4[class^="fl"][class$="title"].fl-module.secondary-heading h5[class^="fl"][class$="title"],.fl-module.secondary-heading h6[class^="fl"][class$="title"],.text-secondary .fl-rich-text p, .secondary-txt, .text-secondary .fl-module-content div[class^="fl-post"][class$="content"] p,.text-secondary .fl-module-content div[class^="fl-post"][class$="excerpt"] p,.text-secondary .fl-module-content div[class^="fl-"][class$="text"] p{
		color:<?php echo $tabbySecondaryColor; ?>
	}
.bg-secondary, .secondary-bg .fl-row-content-wrap, .button-secondary, .fl-builder-content .secondary-btn .fl-module-content .fl-button-wrap a.fl-button,.secondary-btn .fl-module-content div[class^="fl-post-"] a[class^="fl-post-"][class$="more"],.secondary-btn .fl-module-content div[class^="fl-post-"] .fl-post-text .fl-post-more-link a,.fl-builder-content div.secondary-btn .fl-module-content form button[type="submit"],.tabby-block-btn.secondary a, header.header .tabby-top-menu ul li.header-top-btn a, header.header .header-main ul.menu li.menu-item-has-children ul.sub-menu li{
	background-color:<?php echo $tabbySecondaryColor; ?>
}
<?php endif;?>
<?php if (!empty($tabbySecondaryHighlightColor = $tabbyFields['tabby_secondary_highlight_color'])) : ?>
	.text-secondary-highlight .fl-rich-text p, .secondary-txt-highlight, .text-secondary-highlight .fl-module-content div[class^="fl-post"][class$="content"] p,.text-secondary-highlight .fl-module-content div[class^="fl-post"][class$="excerpt"] p,.text-secondary-highlight .fl-module-content div[class^="fl-"][class$="text"] p{
		color:<?php echo $tabbySecondaryHighlightColor; ?>
	}
.bg-secondary-highlight, .secondary-bg-highlight .fl-row-content-wrap, .button-secondary:hover, .button-secondary:focus,.fl-builder-content .secondary-btn .fl-module-content .fl-button-wrap a.fl-button:hover, .secondary-btn .fl-module-content div[class^="fl-post-"] a[class^="fl-post-"][class$="more"]:hover,.secondary-btn .fl-module-content div[class^="fl-post-"] .fl-post-text .fl-post-more-link a:hover,.fl-builder-content div.secondary-btn .fl-module-content form button[type="submit"]:hover,.fl-builder-content .secondary-btn .fl-module-content .fl-button-wrap a.fl-button:focus, .secondary-btn .fl-module-content div[class^="fl-post-"] a[class^="fl-post-"][class$="more"]:focus,.secondary-btn .fl-module-content div[class^="fl-post-"] .fl-post-text .fl-post-more-link a:focus,.fl-builder-content div.secondary-btn .fl-module-content form button[type="submit"]:focus,.tabby-block-btn.secondary-highlight a, header.header .tabby-top-menu ul li.header-top-btn a:hover, {
	background-color:<?php echo $tabbySecondaryHighlightColor; ?>
}
<?php endif;?>
<?php if (!empty($tabbyTertiaryColor = $tabbyFields['tabby_tertiary_color'])) : ?>
	.bg-tertiary,.tertiary-bg .fl-row-content-wrap, .tertiary-bg-post .fl-post-grid .fl-post-grid-post,.tertiary-bg-post .fl-post-feed .fl-post-feed-post .fl-post-feed-text,.tertiary-bg-post .fl-post-carousel .fl-post-carousel-post{
		background-color:<?php echo $tabbyTertiaryColor; ?>
	}
<?php endif;?>
<?php if (!empty($tabbyTertiaryHighlightColor = $tabbyFields['tabby_tertiary_highlight_color'])) : ?>
	.bg-tertiary-highlight,.tertiary-highlight-bg .fl-row-content-wrap, .tertiary-highlight-bg-post .fl-post-grid .fl-post-grid-post,.tertiary-highlight-bg-post .fl-post-feed .fl-post-feed-post .fl-post-feed-text,.tertiary-highlight-bg-post .fl-post-carousel .fl-post-carousel-post{
		background-color:<?php echo $tabbyTertiaryHighlightColor; ?>
	}
<?php endif;?>
<?php if (!empty($breadcrumbLinkColor = $tabbyFields['tabby_breadcrumb_link_color'])) : ?>
	.breadcrumbs-wrap ul li a{
		color: <?php echo $breadcrumbLinkColor;?>;
	}
<?php endif;?>
<?php if (!empty($breadcrumbLinkHoverColor = $tabbyFields['tabby_breadcrumb_link_hover_color'])) : ?>
	.breadcrumbs-wrap ul li a:hover,.breadcrumbs-wrap ul li .current{
		color: <?php echo $breadcrumbLinkHoverColor;?>;
	}
<?php endif;?>
<?php if (!empty($breadcrumbSepColor = $tabbyFields['tabby_breadcrumb_separator_color'])) : ?>
	.breadcrumbs-wrap ul li i{
		color: <?php echo $breadcrumbSepColor;?>;
	}
<?php endif;?>
<?php if (!empty($breadcrumbBgColor = $tabbyFields['tabby_breadcrumb_bg_color'])) : ?>
	.tabby-breadcrumb, .archive-layout-6 .conditions-layout-2 .condition-content-wrapper {
		background-color: <?php echo $breadcrumbBgColor;?>;
	}
<?php endif;?>

<?php if (!empty($archiveBannerBgOverlay = $tabbyFields['tabby_archive_banner_overlay'])):?>
.banner:before{
	background-color:<?php echo $archiveBannerBgOverlay; ?>;
}
<?php endif;?>
<?php if (!empty($archiveBannerOpacity = $tabbyFields['tabby_archive_banner_opacity']) ):?>
.banner:before{
	opacity:<?php echo $archiveBannerOpacity; ?>;
}
<?php endif;?>



.archive-staff .staff-member-item .read-more a,
ul.tabby-social-links li a,
.hip-review-single-wrapper .hip-review-more-link a,
a, .fl-builder-content .fl-button-wrap .fl-button,
.tabby-block-btn a,
.wp-block-button a,
.hip-staff-block-wrapper.layout-grid .hip-staff-block-staff .staff-read-more a.button-primary,
footer.site-footer .sub-footer a,
footer .main-footer .textwidget ul.tabby-social-links li a,
.contact form.wpforms-form .wpforms-submit-container button.wpforms-submit,
.wpforms-submit,
button,
section.location .location-wrapper .map-locations ul li a
{
<?php if(!empty($tabbyLinkColor = $tabbyFields['tabby_link_color'])): ?>
	color: <?php echo $tabbyLinkColor; ?>;
<?php endif; ?>
}
.archive-staff .staff-member-item .read-more a:hover,
header.header .header-top-bar ul.tabby-social-links li a:hover,
.hip-review-single-wrapper .hip-review-more-link a:hover,
a:hover, .fl-builder-content .fl-button-wrap .fl-button:hover,
.tabby-block-btn a:hover,
.hip-staff-block-wrapper.layout-grid .hip-staff-block-staff .staff-read-more a.button-primary:hover,
footer.site-footer .sub-footer a:hover,
footer .main-footer .textwidget ul.tabby-social-links li a:hover,
.contact form.wpforms-form .wpforms-submit-container button.wpforms-submit:hover,
.wpforms-submit:hover,
button:hover,
section.location .location-wrapper .map-locations ul li a:hover
{
<?php if(!empty($tabbyLinkHoverColor = $tabbyFields['tabby_link_hover_color'])): ?>
	color: <?php echo $tabbyLinkHoverColor; ?>;
<?php endif; ?>
}


header.header .tabby-top-menu ul li.header-top-btn a,
.fl-builder-content .fl-button-wrap .fl-button,
.tabby-block-btn a,
.wp-block-button a,
.contact form.wpforms-form .wpforms-submit-container button.wpforms-submit,
.wpforms-submit,
.wpforms-container.wpforms-container-full.booking-now-form .wpforms-submit-container button.wpforms-submit,
button,
.archive-pagination nav.navigation.pagination .nav-links span,
.archive-conditions .conditions-layout-1 .condition-content-wrapper a.button-primary-outline,
footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]
{
<?php if(!empty($tabbyLinkColor = $tabbyFields['tabby_link_color'])): ?>
	background: <?php echo $tabbyLinkColor; ?>;
<?php endif; ?>
}

header.header .tabby-top-menu ul li.header-top-btn a:hover,
.fl-builder-content .fl-button-wrap .fl-button:hover,
.tabby-block-btn a:hover,
.wp-block-button a:hover,
.contact form.wpforms-form .wpforms-submit-container button.wpforms-submit:hover,
.wpforms-submit:hover,
.wpforms-container.wpforms-container-full.booking-now-form .wpforms-submit-container button.wpforms-submit:hover,
button:hover,
footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]:hover,
.archive-conditions .conditions-layout-1 .condition-content-wrapper a.button-primary-outline:hover,
footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]:hover
{
<?php if(!empty($tabbyLinkHoverColor = $tabbyFields['tabby_link_hover_color'])): ?>
	background: <?php echo $tabbyLinkHoverColor; ?>;
<?php endif; ?>
}

/*---------------- fix services layout same as conditions layout ------------------*/
.services-layout-2 {
	padding-top: 50px;
}

.services-layout-2 .single-condition{
	padding-bottom: 50px;
}

.services-layout-2 .condition-content-wrapper {
	padding: 0 20px 50px;
	position: relative;
	height: 100%;
}

.services-layout-2 .condition-content-wrapper .condition-link {
	position: absolute;
	left: 20px;
	bottom: 0;
}

.services-layout-2 .condition-content-wrapper .condition-link a {
	height: auto;
}

@media(max-width: 767px) {
	.services-layout-2 .conditions-content-wrapper {
		padding-left: 10px;
		padding-right: 10px;
	}
}

@media(max-width: 575px) {
	.services-layout-2 .conditions-content-wrapper {
		padding-left: 20px;
		padding-right: 20px;
	}
}

@media(max-width: 575px) {
	.services-layout-2 .condition-content-wrapper .condition-link {
		right: 20px;
	}
}

/* ------------------------------------------------------------------------------ */

<?php
	$btnBgStyle =  $tabbyFields['tabby_btn_style'];
	$btnBgColor =  $tabbyFields['tabby_btn_bg_color'];
	$btnTransparentBgColor =  $tabbyFields['tabby_btn_transparent_bg_color'];
	$btnBgHoverColor =  $tabbyFields['tabby_btn_bg_hover_color'];
	$btnTextColor =  $tabbyFields['tabby_btn_bg_text_color'];
	$btnTextHoverColor =  $tabbyFields['tabby_btn_bg_text_hover_color'];

	$btnBorderBottom =  $tabbyFields['tabby_btn_border_bottom'];
	$btnBorderBottomColor =  $tabbyFields['tabby_btn_border_bottom_color'];
	$btnBorderBottomHoverColor =  $tabbyFields['tabby_btn_border_bottom_hover_color'];
	$btnBorderHeight =  (int)$tabbyFields['tabby_btn_border_btn_height'];

	$btnBorderOutside =  $tabbyFields['tabby_btn_border_outside'];
	$btnBorderOutsideColor =  $tabbyFields['tabby_btn_border_outside_btn_color'];
	$btnBorderOutsideHoverColor =  $tabbyFields['tabby_btn_border_outside_btn_hover_color'];
	$btnBorderOutsideHeight =  (int)$tabbyFields['tabby_btn_border_outside_width'];
?>

<?php if(!empty($btnBgColor)): ?>
	.tabby-block-btn.primary a, .archive-layout-6 a.button-primary-outline, .tabby-service-read-more a.button-primary-outline, .archive-conditions .conditions-layout-1 .condition-content-wrapper a.button-primary-outline, .archive-conditions .conditions-layout-2 .condition-content-wrapper a.button-primary-outline, .services-layout-2 .single-condition .condition-content-wrapper .condition-link a.button-primary-outline, footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit], header.header .tabby-top-menu ul li.header-top-btn a{
		background: <?php echo $btnBgColor; ?>;
	}
	.booking-now-form .wpforms-submit-container button.wpforms-submit, .contact .wpforms-submit-container button.wpforms-submit, footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]{
		background: <?php echo $btnBgColor; ?> !important;
	}
  <?php endif; ?>
<?php if(!empty($btnTransparentBgColor)): ?>
	.tabby-block-btn.primary a, .archive-layout-6 a.button-primary-outline, .tabby-service-read-more a.button-primary-outline, .archive-conditions .conditions-layout-1 .condition-content-wrapper a.button-primary-outline, .archive-conditions .conditions-layout-2 .condition-content-wrapper a.button-primary-outline, .services-layout-2 .single-condition .condition-content-wrapper .condition-link a.button-primary-outline, footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]{
		background: transparent;
	}
	.booking-now-form .wpforms-submit-container button.wpforms-submit, .contact .wpforms-submit-container button.wpforms-submit, footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]{
		background: transparent !important;
	}
<?php endif; ?>
<?php if(!empty($btnBgHoverColor)): ?>
	.tabby-block-btn.primary a:hover, .archive-layout-6 a.button-primary-outline:hover, .tabby-service-read-more a.button-primary-outline:hover, .archive-conditions .conditions-layout-1 .condition-content-wrapper a.button-primary-outline:hover, .archive-conditions .conditions-layout-2 .condition-content-wrapper a.button-primary-outline:hover, .services-layout-2 .single-condition .condition-content-wrapper .condition-link a.button-primary-outline:hover, footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]:hover, header.header .tabby-top-menu ul li.header-top-btn a:hover {
		background: <?php echo $btnBgHoverColor; ?>;
	}
	.booking-now-form .wpforms-submit-container button.wpforms-submit:hover, .contact .wpforms-submit-container button.wpforms-submit:hover, footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]:hover{
		background: <?php echo $btnBgHoverColor; ?>!important;
	}
<?php endif; ?>
<?php if(!empty($btnTextColor)): ?>
	.tabby-block-btn.primary a, .archive-layout-6 a.button-primary-outline, .tabby-service-read-more a.button-primary-outline, .archive-conditions .conditions-layout-1 .condition-content-wrapper a.button-primary-outline, .archive-conditions .conditions-layout-2 .condition-content-wrapper a.button-primary-outline, .services-layout-2 .single-condition .condition-content-wrapper .condition-link a.button-primary-outline, footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit], header.header .tabby-top-menu ul li.header-top-btn a{
		color: <?php echo $btnTextColor; ?>;
	}
	.booking-now-form .wpforms-submit-container button.wpforms-submit, .contact .wpforms-submit-container button.wpforms-submit {
		color: <?php echo $btnTextColor; ?> !important;
	}
<?php endif; ?>
<?php if(!empty($btnTextHoverColor)): ?>
	.tabby-block-btn.primary a:hover, .archive-layout-6 a.button-primary-outline:hover, .tabby-service-read-more a.button-primary-outline:hover, .archive-conditions .conditions-layout-1 .condition-content-wrapper a.button-primary-outline:hover, .archive-conditions .conditions-layout-2 .condition-content-wrapper a.button-primary-outline:hover, .services-layout-2 .single-condition .condition-content-wrapper .condition-link a.button-primary-outline:hover, footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]:hover, header.header .tabby-top-menu ul li.header-top-btn a:hover{
		color: <?php echo $btnTextHoverColor; ?>;
	}
	.booking-now-form .wpforms-submit-container button.wpforms-submit:hover, .contact .wpforms-submit-container button.wpforms-submit:hover {
		color: <?php echo $btnTextHoverColor; ?>!important;
	}
<?php endif; ?>

<?php if($btnBgStyle == 'rectangle'): ?>
	<?php if($btnBorderBottom === 'yes'):?>
		.tabby-block-btn.primary a, .archive-layout-6 a.button-primary-outline,  .tabby-service-read-more a.button-primary-outline, .archive-conditions .conditions-layout-1 .condition-content-wrapper a.button-primary-outline, .archive-conditions .conditions-layout-2 .condition-content-wrapper a.button-primary-outline, .services-layout-2 .single-condition .condition-content-wrapper .condition-link a.button-primary-outline, header.header .tabby-top-menu ul li.header-top-btn a, footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit] {
			border-radius: 0;
			border-top: 0;
			border-right: 0;
			border-left: 0;
			-webkit-transition: all .3s ease;
			-moz-transition: all .3s ease;
			-o-transition: all .3s ease;
			transition: all .3s ease;
			border-style: solid;
			<?php if(!empty($btnBorderBottomColor)): ?>
			border-bottom-color: <?php echo $btnBorderBottomColor; ?>;
			<?php endif; ?>
			<?php if(!empty($btnBorderHeight)): ?>
			border-bottom-width: <?php echo $btnBorderHeight; ?>px;
			<?php else: ?>
			border-bottom-width: 2px;
			<?php endif; ?>
		}
		.booking-now-form .wpforms-submit-container button.wpforms-submit, .contact .wpforms-submit-container button.wpforms-submit {
			border-radius: 0!important;
			border-top: 0!important;
			border-right: 0!important;
			border-left: 0!important;
			-webkit-transition: all .3s ease;
			-moz-transition: all .3s ease;
			-o-transition: all .3s ease;
			transition: all .3s ease;
			border-style: solid!important;
			<?php if(!empty($btnBorderBottomColor)): ?>
			border-bottom-color: <?php echo $btnBorderBottomColor; ?>!important;
			<?php endif; ?>
			<?php if(!empty($btnBorderHeight)): ?>
			border-bottom-width: <?php echo $btnBorderHeight; ?>px!important;
			<?php else: ?>
			border-bottom-width: 2px!important;
			<?php endif; ?>
		}
		.tabby-block-btn.primary a:hover, .archive-layout-6 a.button-primary-outline:hover, .tabby-service-read-more a.button-primary-outline:hover, .archive-conditions .conditions-layout-1 .condition-content-wrapper a.button-primary-outline:hover, .archive-conditions .conditions-layout-2 .condition-content-wrapper a.button-primary-outline:hover, .services-layout-2 .single-condition .condition-content-wrapper .condition-link a.button-primary-outline:hover, header.header .tabby-top-menu ul li.header-top-btn a:hover, footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit]:hover{
			<?php if(!empty($btnBorderBottomHoverColor)): ?>
			border-color: <?php echo $btnBorderBottomHoverColor; ?>;
			<?php endif; ?>
		}
		.booking-now-form .wpforms-submit-container button.wpforms-submit:hover, .contact .wpforms-submit-container button.wpforms-submit:hover{
			<?php if(!empty($btnBorderBottomHoverColor)): ?>
			border-color: <?php echo $btnBorderBottomHoverColor; ?>!important;
			<?php endif; ?>
		}

		footer.site-footer .top-footer.layout-3 div.wpforms-container.newsletter-form form .wpforms-field-container .wpforms-field input[type=email] {
			border-radius: 0;
		}
		footer.site-footer .top-footer.layout-3 div.wpforms-container.newsletter-form form .wpforms-submit-container {
			right: 0;
			top: 10px;
			bottom: 10px;
		}
		footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit] {
	height: 60px;
		}
    <?php else: ?>
		.tabby-block-btn.primary a, .tabby-service-read-more a.button-primary-outline, .archive-conditions .conditions-layout-1 .condition-content-wrapper a.button-primary-outline, .archive-conditions .conditions-layout-2 .condition-content-wrapper a.button-primary-outline, .services-layout-2 .single-condition .condition-content-wrapper .condition-link a.button-primary-outline, header.header .tabby-top-menu ul li.header-top-btn a {
			border-radius: 0;
			border: none;
		}
		.booking-now-form .wpforms-submit-container button.wpforms-submit, .contact .wpforms-submit-container button.wpforms-submit {
			border-radius: 0!important;
			border: none !important;
		}
		footer.site-footer .top-footer.layout-3 div.wpforms-container.newsletter-form form .wpforms-field-container .wpforms-field input[type=email] {
			border-radius: 0;
		}
		footer.site-footer .top-footer.layout-3 div.wpforms-container.newsletter-form form .wpforms-submit-container {
			right: 0;
			top: 10px;
			bottom: 10px;
		}
		footer.site-footer div.wpforms-container.newsletter-form form .wpforms-submit-container button[type=submit] {
			border-radius: 0;
			height: 60px;
		}

	<?php endif; ?>
<?php else: ?>
	.tabby-block-btn.primary a, .tabby-service-read-more a.button-primary-outline, .archive-conditions .conditions-layout-1 .condition-content-wrapper a.button-primary-outline, .archive-conditions .conditions-layout-2 .condition-content-wrapper a.button-primary-outline, .services-layout-2 .single-condition .condition-content-wrapper .condition-link a.button-primary-outline, header.header .tabby-top-menu ul li.header-top-btn a {
		border-radius: 50px;
		-webkit-transition: all .3s ease;
		-moz-transition: all .3s ease;
		-o-transition: all .3s ease;
		transition: all .3s ease;
	<?php if($btnBorderOutside === 'yes'): ?> border-style: solid;
	<?php if(!empty($btnBorderOutsideColor)): ?> border-color: <?php echo $btnBorderOutsideColor; ?>;
	<?php endif; ?> <?php if(!empty($btnBorderOutsideHeight)): ?>
		border-width: <?php echo $btnBorderOutsideHeight; ?>px;
	<?php else: ?> border-width: 2px;
	<?php endif; ?> <?php endif; ?>
	}
	.booking-now-form .wpforms-submit-container button.wpforms-submit, .contact .wpforms-submit-container button.wpforms-submit {
		border-radius: 50px !important;
		-webkit-transition: all .3s ease;
		-moz-transition: all .3s ease;
		-o-transition: all .3s ease;
		transition: all .3s ease;
		<?php if($btnBorderOutside === 'yes'): ?>
		border-style: solid !important;
			<?php if(!empty($btnBorderOutsideColor)): ?>
				border-color: <?php echo $btnBorderOutsideColor; ?> !important;
			<?php endif; ?>
			<?php if(!empty($btnBorderOutsideHeight)): ?>
				border-width: <?php echo $btnBorderOutsideHeight; ?>px!important;
			<?php else: ?>
				border-width: 2px !important;
			<?php endif; ?>
		<?php endif; ?>
	}
	<?php if($btnBorderOutside === 'yes'): ?>
	.tabby-block-btn.primary a:hover, .tabby-service-read-more a.button-primary-outline:hover, .archive-conditions .conditions-layout-1 .condition-content-wrapper a.button-primary-outline:hover, .archive-conditions .conditions-layout-2 .condition-content-wrapper a.button-primary-outline:hover, .services-layout-2 .single-condition .condition-content-wrapper .condition-link a.button-primary-outline:hover, header.header .tabby-top-menu ul li.header-top-btn a:hover{
		<?php if(!empty($btnBorderOutsideHoverColor)): ?>
			border-color: <?php echo $btnBorderOutsideHoverColor; ?>;
		<?php endif; ?>
	}
	.booking-now-form .wpforms-submit-container button.wpforms-submit:hover, .contact .wpforms-submit-container button.wpforms-submit:hover{
		<?php if(!empty($btnBorderOutsideHoverColor)): ?>
			border-color: <?php echo $btnBorderOutsideHoverColor; ?>!important;
		<?php endif; ?>
	}
    <?php endif; ?>
<?php endif; ?>

