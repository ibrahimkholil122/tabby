<?php
global $tabbyFields;
$socialLinkWidth = $tabbyFields['tabby_social_icon_width'];
$socialLinkHeight = $tabbyFields['tabby_social_icon_height'];
$socialLinkFontSize = $tabbyFields['tabby_social_icon_font_size'];
$socialLinkColor = $tabbyFields['tabby_social_icon_color'];
$socialLinkHoverColor = $tabbyFields['tabby_social_icon_hover_color'];
$socialLinkBgColor = $tabbyFields['tabby_social_icon_bg_color'];
$socialLinkBgHoverColor = $tabbyFields['tabby_social_icon_bg_hover_color'];

if(!empty($socialLinkWidth) || !empty($socialLinkHeight) || !empty($socialLinkFontSize) || !empty($socialLinkColor) || !empty($socialLinkBgColor)):
?>
ul.tabby-social-links li a{

	<?php if(!empty($socialLinkWidth)):?>
	width: <?php echo $socialLinkWidth;?>;
	<?php endif;?>
	<?php if(!empty($socialLinkHeight)):?>
	height: <?php echo $socialLinkHeight;?>;
	line-height: <?php echo $socialLinkHeight;?>;
	<?php endif;?>
	<?php if(!empty($socialLinkFontSize)):?>
	font-size: <?php echo $socialLinkFontSize;?>;
	<?php endif;?>
	<?php if(!empty($socialLinkColor)):?>
	color: <?php echo $socialLinkColor;?> !important;
	<?php endif;?>
	<?php if(!empty($socialLinkBgColor)):?>
	background-color: <?php echo $socialLinkBgColor;?>;
	<?php endif;?>
	transition: .2s;
}
<?php endif;?>
<?php if(!empty($socialLinkBgHoverColor) || !empty($socialLinkHoverColor)): ?>
ul.tabby-social-links li a:hover{
	<?php if(!empty($socialLinkHoverColor)):?>
	color: <?php echo $socialLinkHoverColor;?> !important;
	<?php endif;?>
	<?php if(!empty($socialLinkBgHoverColor)):?>
	background-color: <?php echo $socialLinkBgHoverColor;?>;
	<?php endif;?>
	transition: .2s;
}
<?php endif;?>
<?php if($tabbyFields['tabby_sticky_header'] == 'yes'): ?>
body.header-is-sticky #page header.header .header-top-bar ul.tabby-social-links li a{
<?php if(!empty($socialLinkWidth)):?>
	width: <?php echo (rtrim($socialLinkWidth,'px') - (rtrim($socialLinkWidth,'px') * 0.2)).'px';?>;
<?php endif;?>
<?php if(!empty($socialLinkHeight)):?>
	height: <?php echo (rtrim($socialLinkHeight,'px') - (rtrim($socialLinkHeight,'px') * 0.2)).'px';?>;
	line-height: <?php echo (rtrim($socialLinkHeight,'px') - (rtrim($socialLinkHeight,'px') * 0.2)).'px';?>;
<?php endif;?>
<?php if(!empty($socialLinkFontSize)):?>
	font-size: <?php echo (rtrim($socialLinkFontSize,'px') - (rtrim($socialLinkFontSize,'px') * 0.2)).'px';?>;
<?php endif;?>
}
<?php endif; ?>
