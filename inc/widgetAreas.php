<?php
namespace Tabby;
class WidgetAreas{

	public function __construct()
	{
		add_action('widgets_init',[$this,'initWidgetAreas']);
	}
	public function initWidgetAreas()
	{
		$this->footerWidgetAreas();
		$this->pageSidebar();
		$this->postSidebar();
	}
	public function footerWidgetAreas()
	{
		/*above footer*/
		register_sidebar( array(
			'name'          => esc_html__( 'Above Footer', 'tabby' ),
			'id'            => 'above_footer',
			'description'   => esc_html__( 'Add widgets here.', 'tabby' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		));
		/* footer cols */
		for($i = 1; $i <= 4; $i++){
			register_sidebar( array(
				'name'          => esc_html__( 'Main Footer Area-'.$i, 'tabby' ),
				'id'            => 'footer_col_'.$i,
				'description'   => esc_html__( 'Add widgets here.', 'tabby' ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h2 class="widget-title">',
				'after_title'   => '</h2>',
			));
		}
		/*sub footer*/
		register_sidebar( array(
			'name'          => esc_html__( 'Sub Footer', 'tabby' ),
			'id'            => 'sub_footer',
			'description'   => esc_html__( 'Add widgets here.', 'tabby' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		));
	}
	public function pageSidebar()
	{
		register_sidebar( array(
			'name'          => esc_html__( 'Page Sidebar', 'tabby' ),
			'id'            => 'page_sidebar',
			'description'   => esc_html__( 'Add widgets here.', 'tabby' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );
	}
	public function postSidebar()
	{
		register_sidebar( array(
			'name'          => esc_html__( 'Post Sidebar', 'tabby' ),
			'id'            => 'post_sidebar',
			'description'   => esc_html__( 'Add widgets here.', 'tabby' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );
	}
}
new WidgetAreas();