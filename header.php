<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package tabby
 */
global  $tabbyFields;
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
<?php if ( !is_page_template( 'landing-page.php' )): ?>
	<?php $headerLayout = $tabbyFields['tabby_header_layout'];?>
	<!-- Header Layout 1 -->
	<?php
		get_template_part('template-parts/header-'.$headerLayout);
	?>
	<?php if(!is_front_page() && !is_singular( 'post' )):?>
	<?php
		get_template_part('template-parts/banner-'.$headerLayout);
	?>
	<?php endif;?>
	<?php 	if(is_singular( 'post' )) { 
				echo "<hr style='margin-top: 0'>";
			}	
	?>
	<?php endif; ?>

	<!-- End Header Layout-->

	<div id="content" class="site-content">
