<?php
/**
 * 404 page
 * @package tabby
 * @since 1.0.0
 */
get_header();
?>
<div class="container page-with-sidebar-template">
	<div class="row py-4 py-md-5">
		<div class="col-12">
			<div class="main-content py-0 py-md-5">
				<div class="page-404 text-center">
					<h1 class="secondary-txt">404</h1>
					<h5 class="secondary-txt-highlight">Looks Like you're Lost</h5>
					<p>We can't seems to find the page you're looking for</p>
					<div class="btn primary tabby-block-btn">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?> ">Back to Home</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer();?>
