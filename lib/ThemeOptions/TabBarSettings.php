<?php
namespace Tabby\ThemeOptions;
use Carbon_Fields\Field;

global $tabbyFields;

class TabBarSettings
{
	public $settingsFields;

	public function __construct()
	{
		$this->settingsFields = $this->renderSettingsFields();
		add_action('init', [ $this, 'tabbarMenus' ]);
		add_shortcode('tabby-tabbar',[$this,'renderTabbar']);
		add_action('wp_footer',[ $this, 'addTabbarFooter']);
	}
	public function renderSettingsFields()
	{
		return array_merge(
			$this->tab1(),
			$this->tab2(),
			$this->tab3(),
			$this->tab4(),
			$this->tabbarStyles()
		);
	}
	public function tab1()
	{
		return array(
			Field::make( 'separator', 'tabby_tab1_section', __( 'Tab 1' )),
			Field::make( 'text', 'tabby_tab1_name', __( 'Name' ) ),
			Field::make( 'select', 'tabby_tab1_style', __( 'Style' ) )
				->set_options( array(
					'fontawesome' => 'Font Awesome',
					'image' => 'Image',
					'hamburger' => 'Hamburger',
					'genericon' => 'Genericon'

				)),
			Field::make( 'image', 'tabby_tab1_image', 'Upload Image' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab1_style',
						'value' => 'image',
						'compare' => '=',
					)
				)),
			Field::make( 'text', 'tabby_tab1_genericon', 'Genericon Class' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab1_style',
						'value' => 'genericon',
						'compare' => '=',
					)
				)),
			Field::make( 'text', 'tabby_tab1_fontawesome', 'Font Awesome Class' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab1_style',
						'value' => 'fontawesome',
						'compare' => '=',
					)
				)),
			Field::make( 'select', 'tabby_tab1_type', __( 'Type' ) )
				->set_options( array(
					'link' => 'Link',
					'menu' => 'Menu',
				)),
			Field::make( 'text', 'tabby_tab1_link', 'Link Url' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab1_type',
						'value' => 'link',
						'compare' => '=',
					)
				)),

		);
	}
	public function tab2()
	{
		return array(
			Field::make( 'separator', 'tabby_tab2_section', __( 'Tab 2' )),
			Field::make( 'text', 'tabby_tab2_name', __( 'Name' ) ),
			Field::make( 'select', 'tabby_tab2_style', __( 'Style' ) )
				->set_options( array(
					'fontawesome' => 'Font Awesome',
					'image' => 'Image',
					'hamburger' => 'Hamburger',
					'genericon' => 'Genericon'

				)),
			Field::make( 'image', 'tabby_tab2_image', 'Upload Image' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab2_style',
						'value' => 'image',
						'compare' => '=',
					)
				)),
			Field::make( 'text', 'tabby_tab2_genericon', 'Genericon Class' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab2_style',
						'value' => 'genericon',
						'compare' => '=',
					)
				)),
			Field::make( 'text', 'tabby_tab2_fontawesome', 'Font Awesome Class' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab2_style',
						'value' => 'fontawesome',
						'compare' => '=',
					)
				)),
			Field::make( 'select', 'tabby_tab2_type', __( 'Type' ) )
				->set_options( array(
					'link' => 'Link',
					'menu' => 'Menu',
				)),
			Field::make( 'text', 'tabby_tab2_link', 'Link Url' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab2_type',
						'value' => 'link',
						'compare' => '=',
					)
				)),

		);
	}
	public function tab3()
	{
		return array(
			Field::make( 'separator', 'tabby_tab3_section', __( 'Tab 3' )),
			Field::make( 'text', 'tabby_tab3_name', __( 'Name' ) ),
			Field::make( 'select', 'tabby_tab3_style', __( 'Style' ) )
				->set_options( array(
					'fontawesome' => 'Font Awesome',
					'image' => 'Image',
					'hamburger' => 'Hamburger',
					'genericon' => 'Genericon'

				)),
			Field::make( 'image', 'tabby_tab3_image', 'Upload Image' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab3_style',
						'value' => 'image',
						'compare' => '=',
					)
				)),
			Field::make( 'text', 'tabby_tab3_genericon', 'Genericon Class' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab3_style',
						'value' => 'genericon',
						'compare' => '=',
					)
				)),
			Field::make( 'text', 'tabby_tab3_fontawesome', 'Font Awesome Class' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab3_style',
						'value' => 'fontawesome',
						'compare' => '=',
					)
				)),
			Field::make( 'select', 'tabby_tab3_type', __( 'Type' ) )
				->set_options( array(
					'link' => 'Link',
					'menu' => 'Menu',
				)),
			Field::make( 'text', 'tabby_tab3_link', 'Link Url' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab3_type',
						'value' => 'link',
						'compare' => '=',
					)
				)),
		);
	}
	public function tab4()
	{
		return array(
			Field::make( 'separator', 'tabby_tab4_section', __( 'Tab 4' )),
			Field::make( 'text', 'tabby_tab4_name', __( 'Name' ) ),
			Field::make( 'select', 'tabby_tab4_style', __( 'Style' ) )
				->set_options( array(
					'fontawesome' => 'Font Awesome',
					'image' => 'Image',
					'hamburger' => 'Hamburger',
					'genericon' => 'Genericon'
				)),
			Field::make( 'image', 'tabby_tab4_image', 'Upload Image' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab4_style',
						'value' => 'image',
						'compare' => '=',
					)
				)),
			Field::make( 'text', 'tabby_tab4_genericon', 'Genericon Class' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab4_style',
						'value' => 'genericon',
						'compare' => '=',
					)
				)),
			Field::make( 'text', 'tabby_tab4_fontawesome', 'Font Awesome Class' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab4_style',
						'value' => 'fontawesome',
						'compare' => '=',
					)
				)),
			Field::make( 'select', 'tabby_tab4_type', __( 'Type' ) )
				->set_options( array(
					'link' => 'Link',
					'menu' => 'Menu',
				)),
			Field::make( 'text', 'tabby_tab4_link', 'Link Url' )
				->set_conditional_logic( array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_tab4_type',
						'value' => 'link',
						'compare' => '=',
					)
				)),
		);
	}
	public function tabbarStyles()
	{
		return array(
			Field::make( 'separator', 'tabby_tabbar_style_section', __( 'Styles', 'tabby' )),
			Field::make( 'text', 'tabby_hide_windows_larger_than', __( 'Hide on windows larger than', 'tabby' ))->set_default_value('991'),
			Field::make('color', 'tabby_background_color', __('Background color', 'tabby'))->set_width(50),
			Field::make('color', 'tabby_text_link_color', __('Text/Link Color', 'tabby'))->set_width(50),
			Field::make('color', 'tabby_hover_background_color', __('Hover Background Color', 'tabby'))->set_width(50),

			Field::make( 'separator', 'tabby_tabbar_search_section', __( 'Search Bar', 'tabby' )),
			Field::make('checkbox', 'tabby_search_enable', __('Enable ', 'tabby')),
			Field::make('color', 'tabby_search_background_color', __('Background color', 'tabby'))->set_width(50),
			Field::make('color', 'tabby_search_button_text_color', __('Button/Text Color', 'tabby'))->set_width(50),

		);
	}

	public function tabbarMenus()
	{
		register_nav_menus([
			'tab1'	=> 'Tab Bar -- Far Left',
			'tab2'	=> 'Tab Bar -- Center Left',
			'tab3'	=> 'Tab Bar -- Center Right',
			'tab4'	=> 'Tab Bar -- Far Right'
		]);
	}
	public function renderTabbar()
	{   global $tabbyFields;
		ob_start();
		?>
		<div class="tabbar-wrapper <?php if(!empty($tabbyMenuLayout = $tabbyFields['tabby_alternate_menu_layout'])): ?><?php if($tabbyMenuLayout == 'yes'){echo "alternate-menu-style";}?><?php endif;?>">
			<div class="tabbar">
			<?php
				$i=4;
				for ($i=1; $i<=4; $i++){
			?>
			<?php if($tabbyFields['tabby_tab'.$i.'_type'] === 'menu'): ?>
				<div class="tab menu">
			<?php else:	?>
				<div class="tab">
					<a href="<?php echo $tabbyFields['tabby_tab'.$i.'_link']; ?>">
			<?php endif;?>
						<button class="tabbar-button" type="button">
						<?php
						if($tabbyFields['tabby_tab'.$i.'_style'] === 'hamburger'):
						?>
							<p class="c-hamburger c-hamburger--htx"><span></span></p>
						<?php else: ?>
							<div class="tabbar-icon-wrap">
								<?php if($tabbyFields['tabby_tab'.$i.'_style'] === 'image'): ?>
								<?php
									$tabIconImg =  $tabbyFields['tabby_tab'.$i.'_image'];
									$iconImg = wp_get_attachment_image_src($tabIconImg,'small');
									if(!empty($iconImg)):
									echo '<img src="' . $iconImg[0] . '" alt="'.get_bloginfo("name").'">';
									endif;
								?>

								<?php elseif($tabbyFields['tabby_tab'.$i.'_style'] === 'fontawesome'): ?>
									<i class="<?php echo $tabbyFields['tabby_tab'.$i.'_fontawesome']; ?>"></i>
								<?php else: ?>
								<span class="genericon-wrap">
									<?php echo $this->prepareGenericon($tabbyFields['tabby_tab'.$i.'_genericon']); ?>
								</span>
								<?php endif;?>
							</div>
						<?php endif; ?>
							<span class="label"><?php echo $tabbyFields['tabby_tab'.$i.'_name']; ?></span>
						</button>
						<div class="tabbar-menu">

							<?php if(!empty($tabbyFields['tabby_search_enable'])): ?>
							<div class="search-bar">
								<form method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
									<input type="text" class="field" name="s" id="s" placeholder="Search..." />
								</form>
							</div>
							<?php endif; ?>
							<?php wp_nav_menu([ 'theme_location' => "tab$i" ]); ?>
						</div>
					<?php if($tabbyFields['tabby_tab'.$i.'_type'] === 'link'): ?>
					</a>
					<?php endif;?>
				</div>
			<?php } ?>
			</div>
		</div>
		<?php
		return ob_get_flush();
	}
	public function addTabbarFooter() {
		 do_shortcode('[tabby-tabbar]');
	}

	private function prepareGenericon($icon)
	{
		$url = 'href="'.get_template_directory_uri().'/dist/lib/genericons/svg-sprite/';
		preg_match('/href="(.+)"/',$icon,$match);
		if(!empty($match)):
			$url .= $match[1].'"';
		endif;
		return preg_replace('/href="(.+)"/', $url, $icon);


	}
}

