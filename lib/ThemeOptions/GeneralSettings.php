<?php
namespace Tabby\ThemeOptions;
use \Carbon_Fields\Field;

class GeneralSettings
{
	/**
	 * all general settings fields
	 * @var array
	 */
	public $settingsFields;
	/**
	 * method __construct()
	 */
	public function __construct()
	{
		$this->settingsFields = $this->renderSettingsFields();
		add_shortcode('tabby-logo',[$this,'renderLogo']);
		add_shortcode('tabby-alt-logo',[$this,'renderAltLogo']);
	}
	/**
	 * combine settings field to a array
	 * @return array
	 */
	public function renderSettingsFields()
	{
		return array_merge($this->logoSettings(),$this->colorSettings(),$this->breadcrumbSettings(), $this->siteButtonSettings());
	}
	/**
	 * logo settings fields
	 * @return array
	 */
	public function logoSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_logo_section', __( 'Logos', 'tabby') ),
			Field::make( 'image', 'tabby_img_logo', __( 'Main Logo', 'tabby'))
				->set_width( 50 ),
			Field::make( 'image', 'tabby_alt_img_logo', __( 'Alternative Logo', 'tabby'))
				->set_width( 50 )
		);
	}
	/**
	 * color settings fields
	 * @return array
	 */
	public function colorSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_colors_section', __( 'Colors', 'tabby')),
			Field::make( 'color', 'tabby_primary_color', __( 'Primary Color', 'tabby'))->set_width( 50 ),
			Field::make( 'color', 'tabby_primary_highlight_color', __( 'Primary Highlight Color', 'tabby'))->set_width( 50 ),
			Field::make( 'color', 'tabby_secondary_color', __( 'Secondary Color', 'tabby'))->set_width( 50 ),
			Field::make( 'color', 'tabby_secondary_highlight_color', __( 'Secondary Highlight Color', 'tabby'))->set_width( 50 ),
			Field::make( 'color', 'tabby_tertiary_color', __( 'Tertiary Color', 'tabby'))->set_width( 50 ),
			Field::make( 'color', 'tabby_tertiary_highlight_color', __( 'Tertiary Highlight Color', 'tabby'))->set_width( 50 ),
			Field::make( 'color', 'tabby_link_color', __( 'Link Color', 'tabby'))->set_width( 50 ),
			Field::make( 'color', 'tabby_link_hover_color', __( 'Link Hover Color', 'tabby'))->set_width( 50 ),
			Field::make( 'color', 'tabby_body_font_color', __( 'Body Font Color', 'tabby' ))
		);
	}
	/**
	 * breadcrumb settings fields
	 * @return array
	 */
	public function breadcrumbSettings()
	{
		return array(
			Field::make( 'separator', 'tabby_breadcrumb_section', __( 'Breadcrumbs', 'tabby')),
			Field::make( 'color', 'tabby_breadcrumb_link_color', __( 'Link Color', 'tabby'))->set_width( 50 ),
			Field::make( 'color', 'tabby_breadcrumb_link_hover_color', __( 'Link Hover Color', 'tabby'))->set_width( 50 ),
			Field::make( 'color', 'tabby_breadcrumb_separator_color', __( 'Separator Color','tabby'))->set_width( 50 ),
			Field::make( 'color', 'tabby_breadcrumb_bg_color', __( 'Background Color','tabby'))->set_width( 50 )
		);
	}

	/*
	 * site wise button
	 * @return array
	 */
	public function siteButtonSettings(){
		return array(
			Field::make('separator', 'tabby_btn_title', __('Button style')),
			Field::make('select', 'tabby_btn_style', __('Button style'))
				->set_options(array(
					'rectangle' => 'Rectangle',
					'oval' => 'Oval'
				))->set_default_value('oval'),
			Field::make('color', 'tabby_btn_bg_color', __('Button Background'))->set_width(50),
			Field::make('color', 'tabby_btn_bg_hover_color', __('Button Hover Background'))->set_width(50),
			Field::make('checkbox', 'tabby_btn_transparent_bg_color', __('Button Transparent Background')),
			Field::make('color', 'tabby_btn_bg_text_color', __('Button Text Color'))->set_width(50),
			Field::make('color', 'tabby_btn_bg_text_hover_color', __('Button Text Hover Color'))->set_width(50),

			Field::make('select', 'tabby_btn_border_bottom', __('Button border '))
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_btn_style',
						'value' => 'rectangle',
						'compare' => '=',
					)
				))
				->set_options(array(
					'yes' => 'Yes',
					'no' => 'No'
				))->set_default_value('no'),
			Field::make('color', 'tabby_btn_border_bottom_color', __('Border bottom Color'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_btn_style',
						'value' => 'rectangle',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_btn_border_bottom',
						'value' => 'yes',
						'compare' => '=',
					)
				)),

			Field::make('color', 'tabby_btn_border_bottom_hover_color', __('Border bottom Hover Color'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_btn_style',
						'value' => 'rectangle',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_btn_border_bottom',
						'value' => 'yes',
						'compare' => '=',
					)
				)),
			Field::make('text', 'tabby_btn_border_btn_height', __('Border bottom Height'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_btn_style',
						'value' => 'rectangle',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_btn_border_bottom',
						'value' => 'yes',
						'compare' => '=',
					)
				)),

//			outside border style
			Field::make('select', 'tabby_btn_border_outside', __('Outside border'))
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_btn_style',
						'value' => 'oval',
						'compare' => '=',
					)
				))
				->set_options(array(
					'yes' => 'Yes',
					'no' => 'No'
				))->set_default_value('no'),
			Field::make('color', 'tabby_btn_border_outside_btn_color', __('Border  Color'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_btn_style',
						'value' => 'oval',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_btn_border_outside',
						'value' => 'yes',
						'compare' => '=',
					)
				)),

			Field::make('color', 'tabby_btn_border_outside_btn_hover_color', __('Border Hover Color'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_btn_style',
						'value' => 'oval',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_btn_border_outside',
						'value' => 'yes',
						'compare' => '=',
					)
				)),
			Field::make('text', 'tabby_btn_border_outside_width', __('Border Height'))
				->set_width(50)
				->set_conditional_logic(array(
					'relation' => 'AND',
					array(
						'field' => 'tabby_btn_style',
						'value' => 'oval',
						'compare' => '=',
					),
					array(
						'field' => 'tabby_btn_border_outside',
						'value' => 'yes',
						'compare' => '=',
					)
				)),


		);

	}


	/**
	 * callback for 'tabby-logo' shortcode
	 *  @uses _getLogo() method
	 * @return mixed
	 */
	public function renderLogo()
	{
		ob_start();
		?>
		<div class="logo-wrap">
			<a href="<?php echo get_bloginfo('url'); ?>">
				<?php echo $this->_getLogo(); ?>
			</a>
		</div>
		<?php
		return ob_get_clean();
	}
	/**
	 * callback for 'tabby-alt-logo' shortcode
	 * @uses _getLogo() method
	 * @return mixed
	 */

	public function renderAltLogo()
	{
		ob_start();
		?>
		<div class="logo-wrap">
			<a href="<?php echo get_bloginfo('url'); ?>">
				<?php echo $this->_getLogo(true); ?>
			</a>
		</div>
		<?php
		return ob_get_clean();
	}
	/**
	 * get logo from theme options
	 * @param bool
	 * @return mixed
	 */
	private function _getLogo($alt = false)
	{   global $tabbyFields;
		if ($alt === false) {
			$imgLogo =  $tabbyFields['tabby_img_logo'];
			if(!empty($imgLogo)){
				$image = wp_get_attachment_image_src($imgLogo,'medium');
				return '<img src="' . $image[0] . '" alt="'.get_bloginfo("name").'">';
			}else{
				return '<h3>Your logo</h3>';
			}
		} else {
			$altImgLogo = $tabbyFields['tabby_alt_img_logo'];
			if(!empty($altImgLogo)){
				$image = wp_get_attachment_image_src($altImgLogo,'medium');
				return '<img src="' . $image[0] . '" alt="'.get_bloginfo("name").'">';
			}else{
				return '<h3>Your logo</h3>';
			}
		}
	}
}