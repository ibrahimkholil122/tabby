<?php
namespace Tabby;

use \Carbon_Fields\Carbon_Fields;
use \Carbon_Fields\Container;
use Tabby\ThemeOptions\FooterSettings;
use Tabby\ThemeOptions\GeneralSettings;
use Tabby\ThemeOptions\BusinessSettings;
use Tabby\ThemeOptions\LayoutSettings;
use Tabby\ThemeOptions\TabBarSettings;
use Tabby\ThemeOptions\HeaderSettings;

class ThemeOptions
{

	/**
	 * theme options page container instance
	 */
	public $themeOptionsPage;
	/**
	 * method __construct()
	 */
	public function __construct()
	{
		add_action('carbon_fields_register_fields', [$this,'registerThemePage']);
		add_action('carbon_fields_register_fields', [$this,'initThemeOptionsTabs']);
		add_action('after_setup_theme', [$this,'bootCarbonFields']);
	}
	/**
	 * boot carbon fields
	 * @return void
	 */
	public function bootCarbonFields()
	{
		Carbon_Fields::boot();
	}
	/**
	 * register theme options page
	 * set $themeOptionsPage property value
	 * @return void
	 */
	public function registerThemePage()
	{
		$this->themeOptionsPage = Container::make('theme_options', 'tabby_theme_options', __('Theme Options'));
	}
	/**
	 * register tabs in theme options page
	 * @return void
	 */
	public function initThemeOptionsTabs()
	{    global  $tabbyFields;
		/* General settings options */
		$this->themeOptionsPage->add_tab(__('General'), (new GeneralSettings())->settingsFields);
		/* Business settings options */
		$this->themeOptionsPage->add_tab(__('Business Info'), (new BusinessSettings())->settingsFields);
		/* Layout settings options */
		$this->themeOptionsPage->add_tab(__('Layout'), (new LayoutSettings())->settingsFields);
		/* Header settings options */
		$this->themeOptionsPage->add_tab(__('Header'), (new HeaderSettings())->settingsFields);
		/* Footer settings options */
		$this->themeOptionsPage->add_tab(__('Footer'), (new FooterSettings())->settingsFields);

		$menuLayout = carbon_get_theme_option('tabby_header_layout');
		if (!empty($menuLayout) && $menuLayout != 'hamburger') {
			$this->themeOptionsPage->add_tab(__('Tab Bar'), (new TabBarSettings())->settingsFields);
		}
	}
}
