
window.addEventListener('load', function(){

	const  url =  window.location.href;

	// Function will run if match the url.
	if(url.includes("crb_carbon_fields_container_tabby_theme_options")){

		// Tab list query
		const allTabsItem = document.querySelectorAll('#theme-options-form ul.cf-container__tabs-list li');
		// Field query in tab
		const allTabFields = document.querySelectorAll('#theme-options-form .cf-container-theme-options > .cf-container__fields');

		// Save Changes click action
		document.querySelector("#theme-options-form #publish").addEventListener("click", function () {

			allTabFields.forEach((single_tab_fields, index)=>{

				if(getComputedStyle(single_tab_fields, null).display === 'flex'){

					// Save current tab in browser
					localStorage.setItem('targetTab', index);
				}

			})

		});



		if (localStorage.getItem("targetTab") != null) {


			allTabFields.forEach((tabField, index)=>{

				if(localStorage.getItem("targetTab") == index){
					tabField.removeAttribute('hidden');
				}else{
					tabField.setAttribute('hidden', '')
				}

			});



			allTabsItem.forEach((single_tab_item, index)=>{
				
				single_tab_item.addEventListener('click', function(){

					allTabsItem.forEach((item)=>{
						item.classList.remove('cf-container__tabs-item--current');
					});

					allTabFields.forEach((item, fieldindex)=>{
						if(fieldindex == index){
							item.removeAttribute('hidden');
						}else{
							item.setAttribute('hidden', '');
						}
					});

					this.classList.add("cf-container__tabs-item--current");

				});

				if(localStorage.getItem("targetTab") == index){
					single_tab_item.classList.add("cf-container__tabs-item--current");
				}else{
					single_tab_item.classList.remove('cf-container__tabs-item--current');
				}


			});

		 }

	}

});



