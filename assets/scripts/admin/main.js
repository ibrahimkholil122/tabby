(function ($,window) {
	$(window).on('load',function(){
		var postTypeField = $('select[name="hip_post_block_post_type"]');
		if(postTypeField.length > 0){
			var postType = postTypeField.val(),
				data = {
					'action': 'get_post_type_terms',
					'postType': postType,
					'withSelected': 1
				};
			$.post(window.ajaxurl, data, function(r) {
				var response = $.parseJSON(r);
				if(response.status.toString() === 'has_terms'){
					var terms = response.terms,
						options = '',
						postCatField = $('select[name="hip_post_block_post_cats"]'),
						label = postCatField.parents('.cf-field').find('label');;
					options += '<option value=0>All</option>';
					$.each(terms,function(index,value){
						var selected = '';
						if(response.hasOwnProperty('selected')){
							if(response.selected.toString() === value.id.toString()){
								selected = 'selected'
							}
						}
						options += '<option value="'+value.id+'" '+selected+'>'+value.name+'</option>';
					});
					postCatField.html(options);
					label.html(response.taxonomy_label);
				}
			});
		}
	});
	$(document).on('change','select[name="hip_post_block_post_type"]',function(){
		var postType = $(this).val(),
			data = {
			'action': 'get_post_type_terms',
			'postType': postType
		};
		$.post(window.ajaxurl, data, function(r) {
			var response = $.parseJSON(r);
			if(response.status.toString() === 'has_terms'){
				var terms = response.terms,
					options = '',
					postCatField = $('select[name="hip_post_block_post_cats"]'),
					label = postCatField.parents('.cf-field').find('label');
				options += '<option value=0>All</option>';
				$.each(terms,function(index,value){
					options += '<option value="'+value.id+'">'+value.name+'</option>';
				});
				postCatField.html(options);
				label.html(response.taxonomy_label);
			}
		});
	});
	$(document).on('click','.editor-post-publish-button',function () {
		var postBlockCatField = $('select[name="hip_post_block_post_cats"]');
		if(postBlockCatField.length > 0){
			var data = {
				'action': 'save_post_block_category',
				'categoryId': postBlockCatField.val()
			};
			$.post(window.ajaxurl, data);
		}


	})
})(jQuery,window);